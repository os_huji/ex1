matanmo, eyalc
Matan Modai (305097560), Eyal Cohen (301300372)
EX: 1

FILES:
myfile.c -- a file with some code
myfile.h -- a file with some headers

REMARKS:
These are some remarks that
I want the graders to know
about this submission.

ANSWERS:


Q1:
We run strace as follows:
strace -s 1500 WhatIDo 1

The output:
========
brk(0)                                  = 0x1509000
brk(0x153b000)                          = 0x153b000
mkdir("Welcome", 0775)                  = 0
mkdir("Welcome/To", 0775)               = 0
open("Welcome/To/OS", O_WRONLY|O_CREAT|O_TRUNC, 0666) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=0, ...}) = 0
mmap(NULL, 65536, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) =
0x7f946626a000
write(3, "If you didn't read the course guidelines, please do it now!\n
eyalc\n1", 177) = 177
close(3)                                = 0
munmap(0x7f946626a000, 65536)           = 0
unlink("Welcome/To/OS")                 = 0
rmdir("Welcome/To/")                    = 0
rmdir("Welcome/")                       = 0
exit_group(0)                           = ?
+++ exited with 0 +++
=========

The program creates two directories: "Welcome" and "To". Then it creates a file
named OS and writes the following message to it:
"If you didn't read the course guidelines, please do it now!\n<<User>>\n
<<Parameter>>" where <<User>> is replaced by the user name of current logged in
user, and <<Parameter>> is replaced by the given parameter.
At the end, the file and folders are been deleted, and the program ends.

Detailed system calls description:

line 03:
-
mkdir("Welcome", 0775) = 0
-
Creates new directory, named "Welcome" and sets it's permissions to
0775. 0755 means User and Group can read, write and execute while
Other cannot write. the return value "0" means the call ended
succesfully.
Return value 0 on success like in this case and -1 on error.

line 04:
-
mkdir("Welcome/To", 0775) = 0
-
Same as the previous line but now creates a directory named "To" and it is
created inside "Welcome" folder.
Return value 0 on success like in this case and -1 on error.

line 05:
-
open("Welcome/To/OS", O_WRONLY|O_CREAT|O_TRUNC, 0666) = 3
-
Creates a new file called "OS" in path: "Welcome/To". the given flags
are:
* O_WRONLY: opening the file as "write-only"
* O_CREAT: If the file does not exist, it will be created.
* O_TRUNC: If the file already exists and is a regular file
and the access mode allows writing it will be truncated to length 0
The third parameter sets file permissions to 0666 which means:
Anybody can read and write, nobody can execute.
The return value is a file descriptor (smallest available int), in
this case: 3. We can now use it as "reference" for the file.

line 06:
-
fstat(3, {st_mode=S_IFREG|0644, st_size=0, ...}) = 0
-
fstat retrieves information about the file referenced by the given
fd. The status is written in a struct. st_mode=S_IFREG|0644 means regular
file with permission 0644. st_size=0 means no total size is 0 bytes.
return value 0 means success.

line 07:
-
mmap(NULL, 65536, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) =
0x7f946626a000
-
mmap creates a new mapping to the virtual address space of the calling process.
In other words, it allocated memory.
* NULL means the kernel chooses the address where to create the mapping.
* lenthint specifies the length of the mapping. In this case the length is
65536.
* the rest of the flags are other parameters about the way of performing the
mapping.
Returns the address of the beginning of the new mapping when mapping is
successful, on failure returns -1.

line 8:
-
write(3, "If you didn't read the course guidelines, please fo it now!\n
eyalc\n1", 179) = 179
-
Writes the given string to the given file, in this case the file we've created
earlier at line 5.
* 3 is the fd of the file to which we wish to write.
* "If you..." is the string or pointer to what we wish to write.
* 179 is the number of bytes we wish to write to the file.
Return value 179 is the number of bytes that were written successfully.

line 9:
-
close(3) = 0
- 
Closes the file that was opened and to which we wrote earlier.
* 3 is the file descriptor of the file to be closed.
Return value 0 means the file was closed successfully. Return value -1 means an
error has occurred.

line 10:
-
munmap(0x7f573bbce000, 65536) = 0
-
Deletes the mapping at the given address according to the given number of bytes.
* 0x7f573bbce000 is the address where the unmapping is started.
* 65536 is the number of bytes to unmap from starting from the address given in
the first argument.
Returns value 0 when unmapping is successful and -1 on failure.

line 11:
-
unlink("Welcome/To/OS") = 0
-
Deletes a the file "OS" from the system.
the parameter "Welcome/To/OS" is the file path, and return value 0
means the call ended successfully.

line 12:
-
rmdir("Welcome/To/") = 0
-
deletes the "To" directory. gets a pathname and deletes the directory in it.
on success returns 0

line 13:
-
rmdir("Welcome/") = 0
-
deletes the "Welcome" directory. gets a pathname and deletes the directory in it
on success returns 0

line 14:
-
exit_group(0) = ?
-
exit all threads in a process. the parameter is the return status. in
this case it's 0 and that means no error.
this system call does not return any value.


Q2:

Design:
=======
The main goal was to make good measurments, so we had to take some ugly design
decisions. The main problem is that using functions to reduce code duplication
could change measurments, so we just copy and paste most of the code to each
task.

Implementation:
===============

osm_operation_time
------------------
We used & as the simple operation. we defined 5 different register int and
made operations on them, in a manner that each operation depands on the
previuos one, so it's harder for the compiler to optimize it.

osm_function_time
-----------------
Nothing too smart here, we just call an empty function.

osm_syscall_time
----------------
We call the givven empty system call in a loop.

osm_disk_time
-------------
This was the most problematic measurement. We open a file, write a single char
to it * iteration times, and than close and delete the file. The main thing we
tried to avoid is the system use of cache to optimize wiritng. We've added the
flag O_SYNC to the openat command, which is equivilent to calling fsync() after
each write. basically it forces the system to actually write to the disk and
not only to some cache that will be writen later. It's safer to also use
O_DIRECT for this but for unknown reason openat fails when that flag is set.