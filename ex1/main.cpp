//
//  main.cpp
//  ex1
//
//  Created by Eyal Cohen on 24/02/2016.
//  Copyright © 2016 Eyal Cohen. All rights reserved.
//

#include <iostream>
#include <sys/time.h>
#include "osm.h"
//for gethostname
#include <unistd.h>

int main(int argc, const char * argv[]) {

    osm_init();
    measureTimes(100000, 1000, 1000, 2);
    osm_finalizer();

    return 0;
}