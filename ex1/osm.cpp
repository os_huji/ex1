#include <iostream>
#include "osm.h"
#include <sys/time.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>



// TODO ***check exit status***


const double LOOP_UNROLL_DIVIDER = 5;
const double S_TO_NANO = 1000000000;
const double M_TO_NANO = 1000;
const double HOST_NAME_LENGTH = 50;
const double DEFAULT_VAL = -1;
char * hostName;



#define SIMPLE_OPERATION 1 & 1;
#define FILE_NAME ".f"


/**
 * Final calculations to return the times measures
 */
double timesCalc(double sec1, double micro1, double sec2, double micro2,
                 unsigned int iterations)
{
    /* Converting the seconds and microseconds to nanoseconds */
    double secInNano = (double) (sec2 - sec1) *
        (S_TO_NANO / (double) (iterations));
    double microInNano = (double) (micro2 - micro1) *
        (M_TO_NANO / (double) (iterations));

    return (double) (secInNano + microInNano);
};

/**
 * Empty function
 */
void emptyFunc(){};

/* Initialization function that the user must call
 * before running any other library function.
 * The function may, for example, allocate memory or
 * create/open files.
 * Returns 0 uppon success and -1 on failure
 */
int osm_init()
{
    hostName = (char *) malloc(sizeof(char) * 50);

    if(!hostName){
        exit(-1);
    }

    return 0;
};

/* finalizer function that the user must call
 * after running any other library function.
 * The function may, for example, free memory or
 * close/delete files.
 * Returns 0 uppon success and -1 on failure
 */
int osm_finalizer()
{
    free(hostName);
    return 0;
};


/* Time measurement function for a simple arithmetic operation.
   returns time in nano-seconds upon success,
   and -1 upon failure.
   */
double osm_operation_time(unsigned int iterations)
{
    if(!iterations)
    {
        return DEFAULT_VAL;
    }

    /* Small math trick for rounding up */
    unsigned int iter = 1 + ((iterations-1) / LOOP_UNROLL_DIVIDER);


    /* Init variables */
    register int a1, a2, a3, a4, a5 = 0 ;

    timeval tim;
    gettimeofday(&tim, NULL);
    double sec1 = tim.tv_sec;
    double micro1 = tim.tv_usec;


    /* Loop unroll runs operation 5 times */
    for(unsigned int i = 0; i < iter; ++i)
    {
        a1 = a5 & 1;
        a2 = a1 & 1;
        a3 = a2 & 1;
        a4 = a3 & 1;
        a5 = a4 & 1;
    }

    gettimeofday(&tim, NULL);
    double sec2 = tim.tv_sec;
    double micro2 = tim.tv_usec;

    return timesCalc(sec1, micro1, sec2, micro2, iterations);
};

/* Time measurement function for an empty function call.
   returns time in nano-seconds upon success,
   and -1 upon failure.
   */
double osm_function_time(unsigned int iterations=1000)
{
    if(!iterations)
    {
        return DEFAULT_VAL;
    }

    /* Small math trick for rounding up */
    unsigned int iter = 1 + ((iterations-1)/LOOP_UNROLL_DIVIDER);

    timeval tim;
    gettimeofday(&tim, NULL);
    double sec1 = tim.tv_sec;
    double micro1 = tim.tv_usec;

    /* Loop unroll runs operation 5 times */
    for(unsigned int i = 0; i < iter; ++i)
    {
        emptyFunc();
        emptyFunc();
        emptyFunc();
        emptyFunc();
        emptyFunc();
    }

    gettimeofday(&tim, NULL);
    double sec2 = tim.tv_sec;
    double micro2 = tim.tv_usec;

    return timesCalc(sec1, micro1, sec2, micro2, iterations);
}

/* Time measurement function for an empty trap into the operating system.
   returns time in nano-seconds upon success,
   and -1 upon failure.
   */
double osm_syscall_time(unsigned int iterations)
{
    if(!iterations)
    {
        return DEFAULT_VAL;
    }

    /* Small math trick for rounding up */
    unsigned int iter = 1 + ((iterations-1)/LOOP_UNROLL_DIVIDER);

    timeval tim;
    gettimeofday(&tim, NULL);
    double sec1 = tim.tv_sec;
    double micro1 = tim.tv_usec;

    /* Loop unroll runs operation 5 times */
    for(unsigned int i = 0; i < iter; ++i)
    {
        OSM_NULLSYSCALL;
        OSM_NULLSYSCALL;
        OSM_NULLSYSCALL;
        OSM_NULLSYSCALL;
        OSM_NULLSYSCALL;
    }

    gettimeofday(&tim, NULL);
    double sec2 = tim.tv_sec;
    double micro2 = tim.tv_usec;

    return timesCalc(sec1, micro1, sec2, micro2, iterations);
};

/* Time measurement function for accessing the disk.
   returns time in nano-seconds upon success,
   and -1 upon failure.
   */
double osm_disk_time(unsigned int iterations)
{
    if(!iterations)
    {
        return DEFAULT_VAL;
    }

    /* Small math trick for rounding up */
    unsigned int iter = 1 + ((iterations - 1) / LOOP_UNROLL_DIVIDER);

    /* Init file */
    int dir_fd = open("/tmp", S_IRWXG);
    int f = openat(dir_fd, FILE_NAME, O_SYNC|O_CREAT|O_WRONLY|O_TRUNC, 0777);

    if(f < 0)
    {
        return DEFAULT_VAL;
    }

    timeval tim;
    gettimeofday(&tim, NULL);
    double sec1 = tim.tv_sec;
    double micro1 = tim.tv_usec;
    
    /* Loop unroll runs operation 5 times */
    for(unsigned int i = 0; i < iter; ++i)
    {
        write(f, "a", 1);
        write(f, "a", 1);
        write(f, "a", 1);
        write(f, "a", 1);
        write(f, "a", 1);
    }

    gettimeofday(&tim, NULL);
    double sec2 = tim.tv_sec;
    double micro2 = tim.tv_usec;
    
    close(f);
    
    return timesCalc(sec1, micro1, sec2, micro2, iterations);
};


/**
 * Return Struct with measure times
 */
timeMeasurmentStructure measureTimes (unsigned int operation_iterations,
                                      unsigned int function_iterations,
                                      unsigned int syscall_iterations,
                                      unsigned int disk_iterations)
{
    timeMeasurmentStructure timeStruct;

    /* Init times to -1 */
    timeStruct.instructionTimeNanoSecond = DEFAULT_VAL;
    timeStruct.functionTimeNanoSecond = DEFAULT_VAL;
    timeStruct.trapTimeNanoSecond = DEFAULT_VAL;
    timeStruct.diskTimeNanoSecond = DEFAULT_VAL;
    timeStruct.functionInstructionRatio = DEFAULT_VAL;
    timeStruct.trapInstructionRatio = DEFAULT_VAL;
    timeStruct.diskInstructionRatio = DEFAULT_VAL;

    /* Get hostname */
    gethostname(hostName, HOST_NAME_LENGTH);
    timeStruct.machineName = hostName;

    /* Insert values into struct's members */
    timeStruct.instructionTimeNanoSecond =
        osm_operation_time(operation_iterations);
    timeStruct.functionTimeNanoSecond = osm_function_time(function_iterations);
    timeStruct.trapTimeNanoSecond = osm_syscall_time(syscall_iterations);
    timeStruct.diskTimeNanoSecond = osm_disk_time(disk_iterations);


    /* Check 0 division */
    if(timeStruct.instructionTimeNanoSecond)
    {
        /* Calc ratios */
        timeStruct.functionInstructionRatio =
            timeStruct.functionTimeNanoSecond /
                timeStruct.instructionTimeNanoSecond;
        timeStruct.trapInstructionRatio =
            timeStruct.trapTimeNanoSecond /
                timeStruct.instructionTimeNanoSecond;
        timeStruct.diskInstructionRatio =
            timeStruct.diskTimeNanoSecond /
                timeStruct.instructionTimeNanoSecond;
    }

    return timeStruct;
}